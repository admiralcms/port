FROM mhart/alpine-node:10

RUN apk add --no-cache make gcc g++ python
RUN npm install --unsafe-perm -g node-sass gulp gulp-sass gulp-sourcemaps gulp-rename
RUN npm link gulp@3.9.1 gulp-sass gulp-sourcemaps gulp-rename
RUN touch /Gulpfile.js

CMD ["gulp"]
