FROM php:7.4.1-fpm-buster

# Set some fancy labeling
LABEL maintainer="me@finlaydag33k.nl"

# Update the apt
RUN apt-get update

# Add intl
RUN apt-get install -y \
libicu-dev
RUN docker-php-ext-configure intl
RUN docker-php-ext-install intl

# Add PDO
RUN docker-php-ext-install pdo pdo_mysql

# Add GD
RUN apt-get install -y \
libpng-dev libwebp-dev libjpeg-dev libz-dev libxpm-dev
RUN docker-php-ext-configure gd \
    --with-jpeg \
    --with-xpm \
    --with-webp
RUN docker-php-ext-install gd

# Add APCu
RUN pecl install apcu && docker-php-ext-enable apcu

# Add OPcache
RUN docker-php-ext-install opcache