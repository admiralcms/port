# Installation
Before we start, please install Bash, Docker and Docker Compose on your machine.
Bash should come with most Linux Distros by default and is installable on Windows using something like [WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10) or [Git Bash](https://gitforwindows.org/).

Port can be installed either globally (per-machine) or locally (per-project).  
While we do recommend the latter, depending on your needs, this might not be desirable.  
Currently, this documentation is written only for a local install.  
In order to install Port locally, create a new directory for your app (eg. `MyApp`) and clone Port:

```
$ git clone https://gitlab.com/admiralcms/port.git
```

If everything went right, your app directory should now contain a folder named `port`.  

```
/myapp
└── port/
```

That's it, Port is installed locally!

# Creating a new project
Now that we've installed Port, we, of course, want to create a new project using it.  
All we need to do is run the init command and (after answering some minor questions) it will do it all for us!

```
port/port init
```

Wait for a while and your directory should now contain some additional files!  
That's it, your project is now ready to run!

# Starting the Stack
Okay, so we have Port installed, we have created a project, now what?  
Well, it's fairly simple!  
Run the `start` command and after a few moments your stack is all ready!

```
$ port/port start
```

For more useful commands (like the Cake CLI and Composer), head over to the [command reference](commands.md) to see all the available commands!