# Command Reference
Below is a list of all the commands supported by Port as well as their usage and some examples.  

## Init
Initialized a new project in the current directory.  
Copies over all the default Port files (like config, Docker files and Docker Compose stack).

```
port/port init
```

### Example
```
$ port/port init
```

## Build
Builds a specific docker image, or if not specified, builds all the default Port images.  

```
port/port build [image]
```

### Example
```
# Build the php image
$ port/port build php
```

```
# Build all default images
$ port/port build
```

## Start
Starts the Docker Compose stack.

```
port/port start
```

### Example
```
$ port/port start
Starting website_mysql_1     ... done
Starting website_composer_1     ... done
Starting website_sass_1         ... done
Starting website_php_1          ... done
Starting website_nginx_1        ... done
Attaching to website_mysql_1, website_sass_1, website_composer_1, website_php_1, website_queuesadilla_1, website_nginx_1
sass_1          | [06:41:01] Using gulpfile /Gulpfile.js
sass_1          | [06:41:01] Starting 'default'...
sass_1          | [06:41:01] Starting 'compile'...
composer_1      | Loading composer repositories with package information
composer_1      | Installing dependencies (including require-dev) from lock file
composer_1      | Nothing to install or update
composer_1      | Generating autoload files
...
```

## Php
Grants access to the `php` command running inside the `php` container.  
Useful for when you need to run something with php.

```
port/port php [options]
```

### Example
```
# Get the version of php
$ port/port php -v
PHP 7.3.6 (cli) (built: Jul  3 2019 20:59:35) ( NTS )
Copyright (c) 1997-2018 The PHP Group
Zend Engine v3.3.6, Copyright (c) 1998-2018 Zend Technologies
    with Zend OPcache v7.3.6, Copyright (c) 1999-2018, by Zend Technologies
```

## Help
Shows a help prompt, not very useful if you have this command reference in front of you anyways...

Command:
```
port/port help
```

### Example
```
$ port/port help
Usage: port COMMAND [options]

List of available commands:
cake      CakePHP's CLI
composer  Composer CLI
php       PHP CLI
init      Initialize a new CakePHP project
start     Start the stack (must have ran init first)
build     (Re-)Build one or all of the Docker images
```

## Cake
Grants access to CakePHP's `cake` command.  
Useful for manually running migrations or baking something.

```
port/port cake [options]
```

### Example
```
# Run migrations
$ port/port cake migrations migrate
```

```
# Bake Users model
$ port/port cake bake model Users
```

## Composer
Grants access to the Composer command.

### Example
```
# Require Admiral/Admiral
$ port/port composer require admiral/admiral
```

```
# Update all packages
$ port/port composer update
```