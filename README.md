# Port
CakePHP development stack containing Nginx, PHP, Composer and Sass.  
All that's needed is for your machine to support Bash (Comes out of the box with most GNU+Linux distros and is easily installable on Windows 10) and have Docker running.  

It allows you to get started with developing in little to no time while also providing you with access to Cake and Composer's commands!  
It still requires some manual labour at this moment but feel free to contibute any bugfixes and/or features.  

**Note:** Port is currently only compatible with Docker running on x86_64 processors.

# Quick Start
A Quick Start guide is available in the documentation [here](docs/quick-start.md).

# Installing
A guide to install Port is available in the [Quick Start](docs/quick-start.md).

# Usage
A list of all commands and some examples is available in the [Commands Reference](docs/commands.md).