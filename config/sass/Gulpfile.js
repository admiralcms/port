var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var rename = require("gulp-rename");
var path = require("path");

var compressOutput = false; // set to false to disable output compression

// Create a task to compile our SASS-styles
gulp.task('compile', function() {
  // Create a watch for sass/**/*.scss (any scss file in sass/*)
  return gulp.src('/www/**/webroot/scss/**/*.{scss,sass}', {base : '/'})
    // Define the output style
    .pipe(sass({outputStyle: (compressOutput == true ? 'compressed' : 'expanded')})
    // Log errors
    .on('error', sass.logError))
    // The output destination for our compiled *.scss file
    .pipe(gulp.dest(function(file){
      let rel = path.relative(file.path, file.path.replace('/scss/','/css/'));
      let abs = path.join(path.dirname(file.path),'/',rel);
      file.path = "/";
      return abs;
    }));
});


// Create a task to watch
gulp.task('default',function() {
  // Compile css on start
  gulp.start(['compile']);
  // Watch the sass/ folder for changes
  gulp.watch('/www/**/webroot/scss/**/*.scss',['compile']);
});
